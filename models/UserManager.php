<?php

class UserManager{

    public function getUser($user)
    {
        $query="SELECT* FROM usuarios WHERE id=:id";

        return DB::queryAllSimple($query, ['id'=>$user]);
    }

    public function updateUser($data)
    {
        $query="UPDATE `usuarios` 
                SET
                    `firstname`=:firstname,
                    `lastname`=:lastname,
                    `usuario`=:usuario,
                    `phone_number`=:phone_number
                WHERE id=:id";
        return DB::update($query, $data);
    }

    public function disablesUser($user)
    {
        $query="update usuarios SET enable=0 where id=:id";
        return DB::update($query, ['id'=>$user]);
    }

    public function activeUser($user)
    {
        $query="update usuarios SET enable=1 where id=:id";
        return DB::update($query, ['id'=>$user]);
    }
}
    
    
    
    
    
    
    