$(document).ready(function(){
/*
---------------------------------------------------------------------------

					       LOGIN 

---------------------------------------------------------------------------
*/               
                
	$('#send_contact').click(function(){ 
      var pass = document.getElementById("usuario").value;
      var pass1 = document.getElementById("email_address1").value;
      var pas = document.getElementById("pas").value;
      var pas_confirm = document.getElementById("pas_confirm").value;
      var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
      if( pass !== pass1){
           document.getElementById("error_usuario").innerHTML="Emails are not the same";
           document.getElementById("error_email_address1").innerHTML="Emails are not the same";
           return false;
           }else {
           
           if (regex.test(pass.trim())) {
                     //  $("#id_email").text('');
           } else {
                     $("#error_usuario").text('(*)The format of the email field email is incorrect.');
                     $("#error_email_address1").text('(*) The format of the email field email is incorrect.');
                     return false;
               }   
          } 
      if( pas !== pas_confirm){
                //Si no son iguales
               //alert("Las contrase�as no son iguales");
           document.getElementById("error_pas").innerHTML=" (*) Different Password";
           document.getElementById("error_pas_confirm").innerHTML=" (*) Different Password";

           return false;
           }
       
       
       var form = document.querySelector('form');
             form.addEventListener( 'invalid',function(event){
                   event.preventDefault();
             },true); 
        
       var elemt = [ "usuario",
                     "pas",
                     "company",
                     "phone_number",
                     "firstname",
                     "lastname",
                     "email_address1",
            ];
      
       
       elemt.forEach(myFunction);
       
       function myFunction(item) {
         var elemento =document.getElementById(item);
         if (elemento.checkValidity() ==false && elemento.required){
           document.getElementById("error_"+item+"").innerHTML=" (*)";
       }
       }
    });

      


   $('#mostrar_pass').click(function(){
        //Comprobamos que la cadena NO est� vac�a.
        
        if($(this).hasClass('mdi-eye') && ($("#pass").val() != ""))
        {
        $('#pas').removeAttr('type');
        $('#pas_confirm').removeAttr('type');
        $('#mostrar_pass').addClass('mdi-eye-off').removeClass('mdi-eye');
        $('#mostrar_pass').html('<i class="fa fa-eye-slash"></i>');
        }

        else
        {
        $('#pas').attr('type','password');
        $('#pas_confirm').attr('type','password');
        $('#mostrar_pass').addClass('mdi-eye').removeClass('mdi-eye-off');
        $('#mostrar_pass').html('<i class="fa fa-eye"></i>');
        }
    });


});


function disabledUser(user){
    swal({
        title: "Está seguro ?",
        text: "Desea Desactivar el Usuario",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: "user/disabledUser",
                type: 'post',
                data: {
                    user: user
                },
                cache: false,
                success: function(data) {
                    let datos = JSON.parse(data);
                    if (datos.data == "ok"){
                    console.log(data);
                        swal(
                            "Usuario Desactivado", {
                                icon: "success",
                            }
                        );
                    location.href = "/home";
                    }
                }
            });
           
        } 
        
    });
}

function activeUser(user){
    swal({
        title: "Está seguro ?",
        text: "Desea Activar el Usuario",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: "user/activeUser",
                type: 'post',
                data: {
                    user: user
                },
                cache: false,
                success: function(data) {
                    let datos = JSON.parse(data);
                    if (datos.data == "ok"){
                    console.log(data);
                        swal(
                            "Usuario Activado", {
                                icon: "success",
                            }
                        );
                    location.href = "/home";
                    }
                }
            });
           
        } 
        
    });
}




