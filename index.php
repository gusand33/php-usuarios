<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');



session_start();
//session_destroy();


require_once('config.php');


// Setting internal encoding for string functions
mb_internal_encoding("UTF-8");

// Callback for autoloading controllers and models
function autoloadFunction($class)
{
	//Ends with the string "Controller" ?
    //echo $class . "</br>";
    if (preg_match('/Controller$/', $class))	
        require("controllers/" . $class . ".php");
    else
        require("models/" . $class . ".php");
}

function hiddenMail($email)
    {
    $mail_segments = explode("@", $email);
    $mail_segments[0] = substr($email,0,2).str_repeat("*", strlen($mail_segments[0]));
    $mail_segments[1] = substr($mail_segments[1],0,6).str_repeat("*", strlen($mail_segments[1]));
    return implode("@", $mail_segments);
    }
                        
function hourIsBetween($from, $to, $input) {
    $dateFrom = DateTime::createFromFormat('!H:i', $from);
    $dateTo = DateTime::createFromFormat('!H:i', $to);
    $dateInput = DateTime::createFromFormat('!H:i', $input);
    if ($dateFrom > $dateTo) $dateTo->modify('+1 day');
    return ($dateFrom <= $dateInput && $dateInput <= $dateTo) || ($dateFrom <= $dateInput->modify('+1 day') && $dateInput <= $dateTo);
}
                        
// Registers the callback
spl_autoload_register("autoloadFunction");

// Connects to the database
try {
	//Db::connect("localhost", "root", "root", "usuarios");
	Db::connect(HOST, USER, PASSWORD, DB);	
} catch (Exception $e) {
	echo $e->getMessage();
}

// Creating the router and processing parameters from the user's URL
$router = new RouterController();
$router->process(array($_SERVER['REQUEST_URI']));

// Rendering the view
$router->renderView();