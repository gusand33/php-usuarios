<?php
class UserController extends Controller
{
	public function process($params)
	{
            $url="";
            if(isset($params[0])){
                $url=$params[0];
            }
            switch ($url) {
                case 'edit':
                  $this->editUser();
                    break;
                case 'disabledUser':
                    $this->disabledUser();
                    break;    
                case 'activeUser':
                    $this->activeUser();
                    break;
                default:
                //$this->view = 'login/index';
               
        }
 
    
    }

    public function editUser()
    {
        $user=new UserManager;
        if ($this->isPost()) {

            try {
            
                $usuario=[
                    'id' => $_POST['user']['id'],
                    'firstname' =>  $_POST['user']['firstname'],
                    'lastname' => $_POST['user']['lastname'],
                    'usuario' =>  $_POST['user']['usuario'],
                    'phone_number' =>  $_POST['user']['phone_number'],
                ];
           
                $setUser=$user->updateUser($usuario);
                
                $this->redirect('home');

            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }else{
            $this->view = 'users/edit';

        
           
            $user_id='';
            if(isset($_GET['user']))
            $user_id=$_GET['user'];
            $data_user=$user->getUser($user_id);
    
            $this->data['usuario']= $data_user[0];
        }
    }

    public function disabledUser()
    {
      $user=$_POST['user'];
      $users=new UserManager();
      $data=$users->disablesUser($user);
      $res="error";
      if(isset($data)){
          $res=['data'=>'ok'];
      }
      echo json_encode($res);
      exit();

    }

    public function activeUser()
    {
      $user=$_POST['user'];
      $users=new UserManager();
      $data=$users->activeUser($user);
      $res="error";
      if(isset($data)){
          $res=['data'=>'ok'];
      }
      echo json_encode($res);
      exit();

    }

}