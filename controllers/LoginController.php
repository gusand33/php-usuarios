<?php
class LoginController extends Controller
{
	public function process($params)
	{
            $url="";
            if(isset($params[0])){
                $url=$params[0];
            }
            switch ($url) {
                case 'getlogin':
                  $this->loginUsuario($_POST);
                    break;
                case 'logout':
                        session_destroy();
                        $this->redirect('home');
                        break;
                case 'signup':
                    $this->signUp();
                    break;
                default:
                $this->view = 'login/index';
               
        }
 
    
    }

    public function loginUsuario($data)
    {   

        $user=$_POST['usuario'];
        $pass=$_POST['pas'];

        if(!$user || !$pass){
            $this->redirect('login');
            $_SESSION['error']="Debe ingresar Usuario y Contraseña.";
        }else{
            $new=new LoginManager;
            $data=$new->getLogin($user, $pass);

            if(isset($data['error'])==1){
                $this->redirect('login');
                $_SESSION['error']=$data['mensaje'];
    
            }else{
                $_SESSION['user']=$data;
                unset($_SESSION['error']);
                $this->redirect('./home');
            }
            
        }
    }

    public function signUp()
    {
        $this->view = 'login/formsignup';
        //var_dump($_POST);
        if ($this->isPost()) {
            try {
            
                $usuario=[
                    'firstname' =>  $_POST['firstname'],
                    'lastname' => $_POST['lastname'],
                    'usuario' =>  $_POST['usuario'],
                    'phone_number' =>  $_POST['phone_number'],
                    'pas' =>  password_hash($_POST['pas'], PASSWORD_DEFAULT),
                    'created' => date('Y-m-d H:i:s'),
                ];
           
                //var_dump($usuario);
                $login=new LoginManager;
           
                $setUser=$login->nuevoUsuario($usuario);

                if($setUser){
                    $this->redirect('home');
                }

            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }
    }
    
   
}