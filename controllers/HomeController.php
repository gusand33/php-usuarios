<?php
class HomeController extends Controller
{
	public function process($params)
	{
		//var_dump($_SESSION);
		if($_SESSION['user']){
			$this->view = 'users/index';
			$this->getAllUser();
		}else{
			$this->redirect('login');

		}
		
	}

	public function getAllUser()
	{
		$home=new HomeManager;
		$this->data['usuarios']=$home->getAllUser();
	}
}